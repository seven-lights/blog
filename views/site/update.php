<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Site */


$this->title = 'Название сайта';
$this->params['breadcrumbs'][] = ['label' => 'Сайт', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Настройки сайта';
?>
<div class="site-update">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
