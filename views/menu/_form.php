<?php

use app\models\Menu;
use app\models\Tag;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form">

    <?php $form = ActiveForm::begin([
	    'id' => 'post-form',
	    'fieldConfig' => [
		    'template' => '<div class="row">{label}{input}{error}</div>',
	    ],
	    'enableClientValidation' => true,
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'type')->dropDownList(Menu::$types) ?>

	<?
	$tag_models = Tag::find()
		->where([
			'site_id' => Yii::$app->controller->site->id
		])
		->all();
	$tags = [];
	foreach ($tag_models as $tag) {
		/* @var Tag $tag */
		$tags[ $tag->name ] = $tag->name;
	}

	?>
	<?= $form->field($model, 'category')->dropDownList($tags) //для категорий по тегам ?>
    <?= $form->field($model, 'link')->textInput() //для меню по ссылкам ?>

    <?= $form->field($model, 'number')->dropDownList([]) ?>
	<?
	$max_category = Menu::find()
		->where([
			'type' => Menu::TYPE_CATEGORY,
			'site_id' => Yii::$app->controller->site->id,
		])
		->max('number');
	$max_menu = Menu::find()
		->where([
			'type' => Menu::TYPE_MENU,
			'site_id' => Yii::$app->controller->site->id,
		])
		->max('number');
	$select_category = $select_menu = '';
	foreach (range(1, $max_category + (int)$model->isNewRecord) as $num) {
		$select_category .= '<option value="' . $num . '">' . $num . '</option>';
	}
	foreach (range(1, $max_menu + (int)$model->isNewRecord) as $num) {
		$select_menu .= '<option value="' . $num . '">' . $num . '</option>';
	}
	$this->registerJs('(function($) {
		function type() {
			if($("#menu-type").val() == ' . Menu::TYPE_CATEGORY . ') {
				$(".field-menu-category").show();
				$(".field-menu-link").hide();

				$("#menu-number").empty();
				$("#menu-number").html(\'' . $select_category . '\');
			} else if($("#menu-type").val() == ' . Menu::TYPE_MENU . ') {
				$(".field-menu-link").show();
				$(".field-menu-category").hide();

				$("#menu-number").empty();
				$("#menu-number").html(\'' . $select_menu . '\');
			}
		}
		$("#menu-type").change(function() {
			type();
		});
		type();
	})(jQuery)');
	?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить') ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
