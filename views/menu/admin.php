<?php

use app\models\Menu;
use general\assets\AdminAsset;
use yii\grid\ActionColumn;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

AdminAsset::register($this);

$this->title = 'Меню';
$this->params['breadcrumbs'][] = $this->title;
?>
<p>
	<?= Html::a('Создать пункт', ['create']) ?>
</p>

<?= \yii\grid\GridView::widget([
	'dataProvider' => $dataProvider,
	'layout' => '{items}{pager}',
	'columns' => [
		[
			'attribute' => 'type',
			'value' => function($data) {
				/* @var Menu $data */
				return Menu::$types[ $data->type ];
			},
			'filter' => Menu::$types,
		],
		'name',
		'number',
		[
			'class' => ActionColumn::className(),
			'template' => '{update} {delete}'
		],
	],
]) ?>