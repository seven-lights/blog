<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */

$this->title = 'Изменить пункт: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Управление меню', 'url' => ['admin']];
$this->params['breadcrumbs'][] = 'Изменить пункт';
?>
<div class="menu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
