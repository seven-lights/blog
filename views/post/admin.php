<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */
use app\models\Post;
use general\assets\AdminAsset;
use yii\grid\ActionColumn;

/* @var $this yii\web\View */
AdminAsset::register($this);

$this->title = 'Управление записями';
$this->params['breadcrumbs'][] = $this->title;

/* @var yii\data\ActiveDataProvider $dataProvider */
/* @var app\models\search\PostSearch $searchModel */
echo \yii\grid\GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'layout' => '{items}{pager}',
	'columns' => [
		[
			'attribute' => 'id',
			'filterInputOptions' => ['style' => 'width: 50px;'],
		],
		'title',
		[
			'attribute' => 'status',
			'value' => function($data) {
				/* @var Post $data */
				if($data->status == Post::STATUS_PUBLISHED
				&& $data->publish_at > time()) {
					return Post::$statuses[ Post::STATUS_WAITING ];
				}
				return Post::$statuses[ $data->status ];
			},
			'filter' => Post::$statuses,
		],
		[
			'attribute' => 'publish_at',
			'format' => ['date', 'php:' . Yii::$app->params['dateFormat'] ],
		],
		[
			'class' => ActionColumn::className(),
			'template' => '{update} {delete}'
		],
	],
]);