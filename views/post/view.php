<?php
use yii\helpers\Html;

/* @var $model app\models\Post */
/* @var $this yii\web\View */

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag([
	'name' => 'description',
	'content' => $model->description,
], 'description');
$this->registerMetaTag([
	'name' => 'keywords',
	'content' => $model->keywords,
], 'keywords');

$this->registerJsFile('//yandex.st/highlightjs/7.3/highlight.min.js');
$this->registerCssFile('//yandex.st/highlightjs/7.3/styles/googlecode.min.css');
$this->registerJs('hljs.initHighlightingOnLoad();');
?>
<h1><?= $model->title ?></h1>

<div class="post">
	<? if($model->pic_in_post && $model->picture) {
		echo Html::img(
			Yii::$app->params['contentServer'] . '/images/' . $model->id.'.jpg',
			[
				'align' => 'right',
				'width' => 200,
				'alt' => $model->description,
			]
		);
	} ?>
	<?= $model->text ?>
	<br clear="both">
	<section class="meta">
		<? //@todo тег time ?>
		<p><?= date(Yii::$app->params['dateFormat'], $model->publish_at) ?> | Теги:
			<? $listTags = [];
			/* @var $tag app\models\Tag */
			foreach ($model->tags as $tag) {
				$listTags[]= Html::a($tag->name, ['post/list', 'tag' => $tag->name]);
			}
			echo implode(', ', $listTags); ?>
		</p>
	</section>
	<iframe src="http://comment.seven.loc/api/comment/create-frame" width="100%"></iframe>
</div>