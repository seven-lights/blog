<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */
/* @var $model app\models\Post */
use yii\helpers\Html;

?>
<div class="post">
	<?= Html::a($model->title, ['post/view', 'id'=>$model->id], ['class'=>'h1']); ?>
	<? if($model->picture) {
		echo Html::a(
			Html::img(
				Yii::$app->params['contentServer'] . '/images/' . $model->id.'-min.jpg',
				[
					'align'=>'left',
					'width'=>200,
					'alt' => $model->description,
				]
			),
			['post/view', 'id'=>$model->id]
		);
	}?>
	<?= $model->description ?>
	<br clear="both">
	<section class="meta">
		<? //@todo тег time ?>
		<p><?= date(Yii::$app->params['dateFormat'], $model->publish_at) ?> | Теги:
			<? $listTags = [];
			/* @var $tag app\models\Tag */
			foreach ($model->tags as $tag) {
				$listTags[]= Html::a($tag->name, ['post/list', 'tag' => $tag->name]);
			}
			echo implode(', ', $listTags); ?>
		</p>
	</section>
</div>