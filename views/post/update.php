<?php
/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = 'Изменить запись: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = 'Изменить запись';
?>
<div class="post-update">
    <h1><?= $this->title ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
