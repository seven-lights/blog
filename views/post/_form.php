<?php

use app\models\Post;
use janisto\timepicker\TimePicker;
use vova07\imperavi\Widget;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form">
    <?php $form = ActiveForm::begin([
	    'id' => 'post-form',
	    'options' => [
		    'enctype'=>'multipart/form-data',
	    ],
	    'fieldConfig' => [
		    'template' => '<div class="row">{label}{input}{error}</div>',
	    ],
	    'enableClientValidation' => true,
    ]); ?>

	<?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'nick')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'keywords')->textarea(['rows' => 2, 'maxlength' => 128]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6, 'maxlength' => 250]) ?>

	<? echo $form->field($model, 'text')->widget(Widget::className(), [
		'settings' => [
			'lang' => 'ru',
			'minHeight' => 200,
			'plugins' => [
				'clips',
				'fullscreen'
			]
		]
	]); ?>

	<?= $form->field($model, 'marks')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'status')->dropDownList([
	    Post::STATUS_PUBLISHED => Post::$statuses[ Post::STATUS_PUBLISHED ],
	    Post::STATUS_DRAFT => Post::$statuses[ Post::STATUS_DRAFT ],
    ]) ?>

	<?= TimePicker::widget([
		//'model' => $model,
		'value' => date(Yii::$app->params['dateFormat'], $model->publish_at),
		'attribute' => $model::className() . '[publish_at]',
		'language' => 'ru',
		'mode' => 'datetime',
		'clientOptions'=>[
			'dateFormat' => 'dd.mm.yy',
			'timeFormat' => 'HH:mm',
		]
	]) ?>

	<?= $form->field($model, 'picture', [
		'template' => '<div class="row checkbox">{label}{input}{error}</div>',
	])->checkbox([], false) ?>

	<? if($model->image_is_uploaded) { ?>
		<?= Html::beginTag('div', ['class' => 'row']) ?>
			<?= Html::img('//' . Yii::$app->request->serverName . '/i/post/' . $model->site_id . '/' . $model->id . '-small.jpg') ?>
		<?= Html::endTag('div') ?>
	<? } ?>

	<?= $form->field($model, 'image', [
		'template' => '<div class="row">{input}{error}</div>',
		'options' => [
			'style' => 'display: none;',
		],
	])->fileInput() ?>
	<?
	$this->registerJs('(function($) {
		function picture() {
			if($("#post-picture").prop("checked")) {
				$(".field-post-image").show();
			} else {
				$(".field-post-image").hide();
			}
		}
		$("#post-picture").click(function() {
			picture();
		});
		picture();
	})(jQuery)');
	?>

	<?= $form->field($model, 'pic_in_post', [
		'template' => '<div class="row checkbox">{label}{input}{error}</div>',
		'options' => [
			'checked' => $model->isNewRecord ? 'checked' : null
		],
	])->checkbox([], false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить') ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
