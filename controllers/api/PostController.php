<?php

namespace app\controllers\api;

use app\extensions\ApiController;
use app\models\api\search\PostSearch;
use app\models\Post;
use app\models\Tag;
use app\models\TagPost;
use yii\data\Pagination;
use yii\db\Query;
use yii\web\UploadedFile;

class PostController extends ApiController {
	public $defaultAction = 'list';
	private function createTags($tags, $model) {
		foreach ($tags as $mark) {
			$mark = trim($mark);
			if($tag = Tag::findOne(['name' => $mark])) {
				/* @var $tag Tag */
				$tag->frequency++;
				$tag->save();
			} else {
				$tag = new Tag();
				$tag->loadDefaultValues();
				$tag->name = $mark;
				$tag->site_id = $model->site_id;
				$tag->save();
			}

			$tag_post = new TagPost();
			$tag_post->post_id = $model->id;
			$tag_post->tag_id = $tag->id;
			$tag_post->save();
		}
	}
	private function deleteTags($tags, $model) {
		foreach ($tags as $mark) {
			$mark = trim($mark);
			if ($tag = Tag::findOne(['name' => $mark, 'site_id' => $this->site->id])) {
				/* @var $tag Tag */
				if ($tag->frequency == 1) {
					$tag->delete();
				} else {
					$tag->frequency--;
					$tag->save();
					if($tag_post = TagPost::findOne(['post_id' => $model->id, 'tag_id' => $tag->id])) {
						/* @var $tag_post TagPost */
						$tag_post->delete();
					}
				}
			}
		}
	}
	public function actionCreate() {
		if(\Yii::$app->request->isGet) {
			return $this->sendError(self::ERROR_ILLEGAL_REQUEST_METHOD);
		}
		$model = new Post();
		$model->loadDefaultValues();
		$model->image = UploadedFile::getInstance($model, 'image');

		if(!$model->load(\Yii::$app->request->post())) {
			return $this->sendError(self::ERROR_NO_DATA);
		}

		if($model->validate()) {
			if(!$model->save(false)) {
				return $this->sendError(self::ERROR_DB);
			}
			$model->saveImage();
			//сохраняем метки
			$this->createTags(explode(',', $model->marks), $model);

			$ret = [
				'id' => $model->id,
				'nick' => $model->nick,
				'title' => $model->title,
				'keywords' => $model->keywords,
				'description' => $model->description,
				'text' => $model->text,
				'tags' => [],
				'status' => $model->status,
				'publish_at' => $model->publish_at,
				'picture' => $model->picture,
				'pic_in_post' => $model->pic_in_post,
			];

			foreach ($model->tags as $tag) {
				/* @var Tag $tag */
				$ret['tags'][] = $tag->name;
			}
			if($model['picture']) {
				$ret['image_links'] = Post::getImageLinks($model);
			}

			return $this->sendSuccess(['post' => $ret]);
		} else {
			$error_codes = [
				'title' => self::ERROR_ILLEGAL_TITLE,
				'text' => self::ERROR_ILLEGAL_TEXT,
				'marks' => self::ERROR_ILLEGAL_TAGS,
				'nick' => self::ERROR_ILLEGAL_NICK,
				'keywords' => self::ERROR_ILLEGAL_KEYWORDS,
				'description' => self::ERROR_ILLEGAL_DESCRIPTION,
				'status' => self::ERROR_ILLEGAL_POST_STATUS,
				'picture' => self::ERROR_ILLEGAL_FIELD_PICTURE,
				'pic_in_post' => self::ERROR_ILLEGAL_FIELD_PICTURE_IN_POST,
				'image' => self::ERROR_ILLEGAL_PICTURE,
				'publish_at' => self::ERROR_ILLEGAL_PUBLISH_DATE
			];
			foreach ($model->getFirstErrors() as $attr => $err) {
				if(isset($error_codes[ $attr ])) {
					$errors[] = $error_codes[ $attr ];
				}
			}
		}

		if(!isset($errors)) {
			$errors = self::ERROR_UNKNOWN;
		}
		return $this->sendError($errors);
	}
	public function actionUpdate($id) {
		/* @var Post $model */
		$model = Post::find()
			->where(['id' => $id])
			->andWhere(['site_id' => $this->site->id])
			->with('tags')
			->one();
		if(!$model) {
			return $this->sendError(self::ERROR_NO_POST);
		}
		if(\Yii::$app->request->isPost) {
			$oldMarks = [];
			foreach($model->tags as $val) {
				$oldMarks[] = $val->name;
			}

			if (!$model->load(\Yii::$app->request->post())) {
				return $this->sendError(self::ERROR_NO_DATA);
			}
			$model->image = UploadedFile::getInstance($model, 'image');

			if ($model->validate()) {
				if (!$model->save(false)) {
					return $this->sendError(self::ERROR_DB);
				}
				$model->saveImage();

				$newMarks = [];
				foreach(explode(',', $model->marks) as $val) {
					$newMarks[] = trim($val);
				}

				$diffDelete = array_diff($oldMarks, $newMarks);
				$diffCreate = array_diff($newMarks, $oldMarks);

				$this->createTags($diffCreate, $model);
				$this->deleteTags($diffDelete, $model);
			} else {
				$errors = $this->getErrorCodes([
					'title' => self::ERROR_ILLEGAL_TITLE,
					'text' => self::ERROR_ILLEGAL_TEXT,
					'marks' => self::ERROR_ILLEGAL_TAGS,
					'nick' => self::ERROR_ILLEGAL_NICK,
					'keywords' => self::ERROR_ILLEGAL_KEYWORDS,
					'description' => self::ERROR_ILLEGAL_DESCRIPTION,
					'status' => self::ERROR_ILLEGAL_POST_STATUS,
					'picture' => self::ERROR_ILLEGAL_FIELD_PICTURE,
					'pic_in_post' => self::ERROR_ILLEGAL_FIELD_PICTURE_IN_POST,
					'image' => self::ERROR_ILLEGAL_PICTURE,
					'publish_at' => self::ERROR_ILLEGAL_PUBLISH_DATE
				], $model);
				return $this->sendError($errors);
			}
		}

		$ret = [
			'id' => $model->id,
			'nick' => $model->nick,
			'title' => $model->title,
			'keywords' => $model->keywords,
			'description' => $model->description,
			'text' => $model->text,
			'tags' => [],
			'status' => $model->status,
			'publish_at' => $model->publish_at,
			'picture' => $model->picture,
			'pic_in_post' => $model->pic_in_post,
		];

		foreach ($model->tags as $tag) {
			/* @var Tag $tag */
			$ret['tags'][] = $tag->name;
		}
		if($model['picture']) {
			$ret['image_links'] = Post::getImageLinks($model);
		}

		return $this->sendSuccess(['post' => $ret]);
	}
	public function actionDelete($id) {
		/* @var Post $post */
		$post = Post::find()
			->where(['id' => $id])
			->andWhere(['site_id' => $this->site->id])
			->with('tags')
			->one();
		if($post) {
			$model = clone $post;
			if($post->delete() === false) {
				return $this->sendError(self::ERROR_DB);
			} else {
				if ($model->image_is_uploaded
					&& file_exists(\Yii::getAlias('@app/web/i/post') . '/' . $model->site_id . '/' . $model->id . '.jpg')
				) {
					unlink(\Yii::getAlias('@app/web/i/post') . '/' . $model->site_id . '/' . $model->id . '.jpg');
				}
				$this->deleteTags(explode(',', $model->marks), $model);
				return $this->sendSuccess([]);
			}
		} else {
			return $this->sendError(self::ERROR_NO_POST);
		}
	}
    public function actionList($page = 0) {
	    if($page > 100) {
		    return $this->sendError(self::ERROR_TOO_BIG_PAGE_NUMBER);
	    }

	    $tag = \Yii::$app->request->get('tag');

	    //Считаем кол-во записей
	    if($tag) {
		    $totalCount = Post::find()
			    ->joinWith('tags')
			    ->where(['tag.name' => $tag])
			    ->count();
	    } else {
		    $totalCount = (new Query())->from('post')->count();
	    }
	    if($totalCount == 0) {
		    return $this->sendError(self::ERROR_NO_POST);
	    }
	    $pages = new Pagination(['totalCount' => $totalCount]);
	    $pages->setPage($page);
	    $pages->setPageSize(30);

	    $res = Post::find()
		    ->select('post.id, post.title, post.description, post.picture, post.publish_at, post.site_id')
		    ->where(['status' => Post::STATUS_PUBLISHED])
		    ->andWhere(['<', 'publish_at', time()])
		    ->andWhere(['post.site_id' => $this->site->id])
		    ->with('tags')
		    ->orderBy(['publish_at' => SORT_DESC]);

	    if($tag) {
		    $res
			    ->joinWith('tags')
			    ->andWhere(['tag.name' => $tag]);
	    }

	    $res = $res
		    ->offset($pages->offset)
		    ->limit($pages->limit)
		    ->asArray()
		    ->all();

	    if($res === []) {
		    return $this->sendError(self::ERROR_DB);
	    }

	    foreach ($res as &$val) {
		    $tags = [];
		    foreach ($val['tags'] as $tag) {
			    $tags[] = $tag['name'];
		    }
		    $val['tags'] = $tags;
		    if($val['picture']) {
			    $val['image_links'] = Post::getImageLinks($val);
		    }
	    }

	    return $this->sendSuccess([
		    'posts' => $res,
		    'totalCount' => $totalCount,
		    'page' => $pages->getPage()
	    ]);
    }
	public function actionAdmin($page = 0) {
		if($page > 100) {
			return $this->sendError(self::ERROR_TOO_BIG_PAGE_NUMBER);
		}

		$data = (new PostSearch())->search(\Yii::$app->request->get(), $this->site->id, $page);

		if(is_int($data)) {
			return $this->sendError($data);
		}

		if($data === []) {
			return $this->sendError(self::ERROR_DB);
		}

		return $this->sendSuccess($data);
	}
    public function actionView($id) {
	    $model = Post::find()
		    ->select([
			    'id',
			    'nick',
			    'title',
			    'keywords',
			    'description',
			    'text',
			    'status',
			    'publish_at',
			    'picture',
			    'pic_in_post',
			    'site_id',
		    ])
	        ->where(['id' => $id])
		    ->andWhere(['status' => Post::STATUS_PUBLISHED])
		    ->andWhere(['<', 'publish_at', time()])
		    ->andWhere(['site_id' => $this->site->id])
		    ->with('tags')
		    ->asArray()
	        ->one();
	    $tags = [];
	    foreach ($model['tags'] as $tag) {
		    $tags[] = $tag['name'];
	    }
	    $model['tags'] = $tags;
	    if($model['picture']) {
		    $model['image_links'] = Post::getImageLinks($model);
	    }
	    if(!$model) {
		    return $this->sendError(self::ERROR_NO_POST);
	    }
        return $this->sendSuccess(['post' => $model]);
    }
}
