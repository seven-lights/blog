<?php

namespace app\controllers\api;

use app\extensions\ApiController;
use app\models\api\forms\TagForm;
use app\models\Tag;

class TagController extends ApiController {
	public $defaultAction = 'list';
    public function actionList() {
	    $res = Tag::find()
		    ->select('name, frequency')
		    ->where(['site_id' => $this->site->id])
		    ->orderBy(['frequency' => SORT_DESC])
		    ->limit(1000)
		    ->all();

	    if($res === []) {
		    return $this->sendError(self::ERROR_DB);
	    }

	    $tags = [];
	    foreach ($res as $val) {
		    /* @var Tag $val */
		    $tags[] = [ $val->name => $val->frequency ];
	    }

	    return $this->sendSuccess(['tags' => $tags]);
    }
	public function actionDelete($tag) {
		if($model = Tag::findOne(['name' => $tag, 'site_id' => $this->site->id ])) {
			/* @var $model Tag */
			if($model->delete() === false) {
				return $this->sendError(self::ERROR_DB);
			} else {
				return $this->sendSuccess([]);
			}
		} else {
			return $this->sendError(self::ERROR_NO_TAGS);
		}
	}

	public function actionUpdate($tag) {
		if(!\Yii::$app->request->isPost) {
			return $this->sendError(self::ERROR_ILLEGAL_REQUEST_METHOD);
		}

		/* @var Tag $model */
		if(!$model = TagForm::findOne(['name' => $tag, 'site_id' => $this->site->id ])) {
			return $this->sendError(self::ERROR_NO_TAGS);
		}

		if (!$model->load(\Yii::$app->request->post())) {
			return $this->sendError(self::ERROR_NO_DATA);
		}

		if ($model->validate()) {
			if (!$model->save(false)) {
				return $this->sendError(self::ERROR_DB);
			} else {
				return $this->sendSuccess([]);
			}
		} else {
			$errors = $this->getErrorCodes(['name' => self::ERROR_ILLEGAL_TAGS], $model);
			return $this->sendError($errors);
		}
	}
}
