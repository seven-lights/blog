<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\controllers;


use app\extensions\Controller;
use general\ext\services\page\PageControllerTrait;

class PageController extends Controller {
	use PageControllerTrait;
	public $defaultAction = 'view';
	private static $domain;
	public function __construct($id, $module, $config = []) {
		parent::__construct($id, $module, $config);
		static::$domain = \Yii::$app->request->getServerName();
	}
}