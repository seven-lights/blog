<?php

namespace app\controllers;

use app\extensions\Controller;
use app\models\Post;
use app\models\search\PostSearch;
use app\models\Tag;
use app\models\TagPost;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class PostController extends Controller {
	public $defaultAction = 'list';
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['create', 'delete', 'update', 'admin'],
				'rules' => [
					[
						'actions' => ['create', 'delete', 'update', 'admin'],
						'allow' => true,
						'matchCallback' => function ($rule, $action) {
							/* @var Controller $controller */
							$controller = \Yii::$app->controller;
							return $controller->isAdmin();
						}
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}
	private function createTags($tags, $model) {
		foreach ($tags as $mark) {
			$mark = trim($mark);
			if($tag = Tag::findOne(['name' => $mark])) {
				/* @var $tag Tag */
				$tag->frequency++;
				$tag->save();
			} else {
				$tag = new Tag();
				$tag->loadDefaultValues();
				$tag->name = $mark;
				$tag->site_id = $model->site_id;
				$tag->save();
			}

			$tag_post = new TagPost();
			$tag_post->post_id = $model->id;
			$tag_post->tag_id = $tag->id;
			$tag_post->save();
		}
	}
	private function deleteTags($tags, $model) {
		foreach ($tags as $mark) {
			$mark = trim($mark);
			if ($tag = Tag::findOne(['name' => $mark, 'site_id' => $this->site->id])) {
				/* @var $tag Tag */
				if ($tag->frequency == 1) {
					$tag->delete();
				} else {
					$tag->frequency--;
					$tag->save();
					if($tag_post = TagPost::findOne(['post_id' => $model->id, 'tag_id' => $tag->id])) {
						/* @var $tag_post TagPost */
						$tag_post->delete();
					}
				}
			}
		}
	}
	public function actionCreate() {
		$model = new Post();
		$model->loadDefaultValues();

		if (\Yii::$app->request->isPost) {
			$model->image = UploadedFile::getInstance($model, 'image');
			if($model->load(\Yii::$app->request->post()) && $model->save()) {
				$model->saveImage();
				//сохраняем метки
				$this->createTags(explode(',', $model->marks), $model);

				return $this->redirect(['update', 'id' => $model->id]);
			}
		}
		return $this->render('create', [
			'model' => $model,
		]);
	}
	public function actionUpdate($id)
	{
		/* @var Post $model */
		$model = Post::find()
			->where(['id' => $id])
			->andWhere(['site_id' => $this->site->id])
			->with('tags')
			->one();

		if (!$model) {
			throw new NotFoundHttpException();
		}

		if (\Yii::$app->request->isPost) {
			$oldMarks = [];
			foreach($model->tags as $val) {
				$oldMarks[] = $val->name;
			}

			$model->load(\Yii::$app->request->post());
			$model->image = UploadedFile::getInstance($model, 'image');
			if($model->save()) {
				$model->saveImage();

				$newMarks = [];
				foreach(explode(',', $model->marks) as $val) {
					$newMarks[] = trim($val);
				}

				$diffDelete = array_diff($oldMarks, $newMarks);
				$diffCreate = array_diff($newMarks, $oldMarks);

				$this->createTags($diffCreate, $model);
				$this->deleteTags($diffDelete, $model);

				return $this->redirect(['update', 'id' => $model->id]);
			}
		}
		return $this->render('update', [
			'model' => $model,
		]);
	}
	public function actionDelete($id) {
		/* @var Post $post */
		$post = Post::find()
			->where(['id' => $id])
			->andWhere(['site_id' => $this->site->id])
			->with('tags')
			->one();
		$model = clone $post;
		if($post->delete()) {
			if ($model->image_is_uploaded
				&& file_exists(\Yii::getAlias('@app/web/i/post') . '/' . $model->site_id . '/' . $model->id . '.jpg')
			) {
				unlink(\Yii::getAlias('@app/web/i/post') . '/' . $model->site_id . '/' . $model->id . '.jpg');
			}
			$this->deleteTags(explode(',', $model->marks), $model);
		}
		return $this->redirect(['admin']);
	}
    public function actionList() {
	    $query = Post::find()
		    ->select(['post.id', 'post.title', 'post.description', 'post.picture', 'post.publish_at'])
		    ->where(['status' => Post::STATUS_PUBLISHED])
		    ->andWhere(['<', 'publish_at', time()])
		    ->andWhere(['post.site_id' => $this->site->id])
		    ->with('tags')
	        ->orderBy(['publish_at' => SORT_DESC]);

	    if(\Yii::$app->request->get('tag')) {
		    $query
			    ->joinWith('tags')
			    ->andWhere(['tag.name' => \Yii::$app->request->get('tag')]);
	    }

	    $dataProvider = new ActiveDataProvider([
		    'query' => $query,
		    'pagination' => [
			    'pageSize' => 15,
		    ],
	    ]);

        return $this->render('list', [
	        'dataProvider' => $dataProvider,
	        'tag' => \Yii::$app->request->get('tag'),
        ]);
    }
	public function actionAdmin() {
		$searchModel = new PostSearch();
		$dataProvider = $searchModel->search(\Yii::$app->request->get(), $this->site->id);

		return $this->render('admin', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}
    public function actionView($id) {
	    $model = Post::find()
	        ->where(['id' => $id])
		    ->andWhere(['status' => Post::STATUS_PUBLISHED])
		    ->andWhere(['<', 'publish_at', time()])
		    ->andWhere(['site_id' => $this->site->id])
		    ->with('tags')
	        ->one();
	    if(!$model) {
		    throw new NotFoundHttpException();
	    }
        return $this->render('view', [
	        'model' => $model,
        ]);
    }
}
