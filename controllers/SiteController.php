<?php

namespace app\controllers;

use yii\filters\AccessControl;
use app\extensions\Controller;
use app\models\AuthKey;
use app\models\User;
use general\ext\api\auth\AuthApi;
use general\ext\api\auth\AuthUrlCreator;
use Yii;
use yii\web\Response;
use app\models\forms\SiteForm;

class SiteController extends Controller {
    public $defaultAction = 'index';
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionIndex() {
        return $this->render('index');
    }
	/*public function actionSignup() {
		$model = new SignupForm();
		if ($model->load(Yii::$app->request->post())) {
			if ($user = $model->signup()) {
				if (Yii::$app->user->login($user, Yii::$app->params['durationAuth'])) {
					return $this->goHome();
				}
			}
		}

		return $this->render('signup', [
			'model' => $model,
		]);
	}*/
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['update'],
				'rules' => [
					[
						'actions' => ['update'],
						'allow' => true,
						'matchCallback' => function ($rule, $action) {
							/* @var Controller $controller */
							$controller = \Yii::$app->controller;
							return $controller->isAdmin();
						}
					],
				],
			],
		];
	}
    public function actionLoginWithAuthKey($result) {
	    $this->layout = 'emptyHtml';
	    if($result == 'success') {
		    if (!Yii::$app->user->isGuest) {
			    return $this->render('redirect', ['url' => '/']);
		    }

		    $mark = Yii::$app->request->get('key');
		    if($key = AuthApi::userGetServiceAuthKey($mark)) {
			    $user = User::getFromAuth($key['user_id']);

			    $model = new AuthKey();
			    $model->auth_key = $key['auth_key'];
			    $model->browser = Yii::$app->request->userAgent;
			    $model->ip = Yii::$app->request->userIP;
			    $model->user_id = $key['user_id'];
			    $model->save();

			    if (Yii::$app->user->login($user, Yii::$app->params['durationAuth'])) {
				    return $this->render('refresh');
			    }
		    } else {
			    //@todo сделать вывод ошибки
		    }
	    }
	    return $this->render('redirect', ['url' => '/']);
    }
	public function actionLoginWithMark($mark) {
		if (!Yii::$app->user->isGuest) {
			return '';
		}

		\Yii::$app->response->format = Response::FORMAT_JSONP;
		if($key = AuthApi::userGetServiceAuthKey($mark)) {
			$user = User::getFromAuth($key['user_id']);

			$model = new AuthKey();
			$model->auth_key = $key['auth_key'];
			$model->browser = Yii::$app->request->userAgent;
			$model->ip = Yii::$app->request->userIP;
			$model->user_id = $key['user_id'];
			$model->save();

			if (Yii::$app->user->login($user, Yii::$app->params['durationAuth'])) {
				return [
					'data' => 'success',
					'callback' => 'loginWithMark',
				];
			}
		}
		return [
			'data' => 'error',
			'callback' => 'loginWithMark',
		];
	}
	public function actionLogin() {
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		return $this->render('login');
	}
    public function actionLogout() {
        User::logout();
	    return $this->redirect(
		    AuthUrlCreator::userLogout(
			    Yii::$app->id,
			    '//' . Yii::$app->request->getServerName()));
    }
	public function actionLogoutEverywhere() {
		User::logoutEverywhere();
		return $this->redirect(
			AuthUrlCreator::userLogoutEverywhere(
				Yii::$app->id,
				'//' . Yii::$app->request->getServerName()));
	}
	public function actionUpdate()
	{
		/* @var $model SiteForm */
		$model = SiteForm::findOne($this->site->id);

		if (Yii::$app->request->isPost) {
			$model->load(Yii::$app->request->post());
			$model->save();
		}

		return $this->render('update', [
			'model' => $model,
		]);

	}
}