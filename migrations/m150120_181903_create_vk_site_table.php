<?php

use yii\db\Schema;
use yii\db\Migration;

class m150120_181903_create_vk_site_table extends Migration
{
    public function up()
    {
	    $this->createTable('vk_site', [
		    'vk_id' => Schema::TYPE_BIGINT . ' NOT NULL',
		    'site_id' => Schema::TYPE_SMALLINT . ' NOT NULL',
	    ]);
	    $this->addPrimaryKey('primary_key', 'vk_site', ['site_id', 'vk_id']);
    }

    public function down()
    {
        echo "m150120_181903_create_vk_site_table cannot be reverted.\n";

        return false;
    }
}
