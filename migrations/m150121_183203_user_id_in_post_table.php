<?php

use yii\db\Schema;
use yii\db\Migration;

class m150121_183203_user_id_in_post_table extends Migration
{
    public function up()
    {
	    $this->addColumn('post', 'author_id', Schema::TYPE_INTEGER . ' NULL');
	    $this->addForeignKey('author_id_FK_post', 'post', 'author_id', 'user', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        echo "m150121_183203_user_id_in_post_table cannot be reverted.\n";

        return false;
    }
}
