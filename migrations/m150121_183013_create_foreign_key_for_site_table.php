<?php

use yii\db\Schema;
use yii\db\Migration;

class m150121_183013_create_foreign_key_for_site_table extends Migration
{
    public function up()
    {
	    $this->alterColumn('site', 'owner_id', Schema::TYPE_INTEGER . ' NULL');
	    $this->addForeignKey('owner_id_FK_site', 'site', 'owner_id', 'user', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        echo "m150121_183013_create_foreign_key_for_site_table cannot be reverted.\n";

        return false;
    }
}
