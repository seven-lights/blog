<?php

use yii\db\Schema;
use yii\db\Migration;

class m150121_182706_create_foreign_key_for_post_table extends Migration
{
    public function up()
    {
	    $this->addForeignKey('site_id_FK_post', 'post', 'site_id', 'site', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m150121_182706_create_foreign_key_for_post_table cannot be reverted.\n";

        return false;
    }
}
