<?php

use yii\db\Schema;
use yii\db\Migration;

class m150121_182938_create_foreign_key_for_redactor_table extends Migration
{
    public function up()
    {
	    $this->addForeignKey('site_id_FK_redactor', 'redactor', 'site_id', 'site', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('user_id_FK_redactor', 'redactor', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m150121_182938_create_foreign_key_for_redactor_table cannot be reverted.\n";

        return false;
    }
}
