<?php

use yii\db\Schema;
use yii\db\Migration;

class m150306_163803_add_unique_index_in_tag_table extends Migration
{
    public function up()
    {
	    $this->createIndex('tag_tbl_name_site_id_idx', 'tag', ['name', 'site_id'], true);
    }

    public function down()
    {
        echo "m150306_163803_add_unique_index_in_tag_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
