<?php

use yii\db\Schema;
use yii\db\Migration;

class m150118_153807_create_post_table extends Migration
{
    public function up()
    {
	    $this->createTable('post', [
		    'id' => Schema::TYPE_PK,
		    'nick' => Schema::TYPE_STRING . '(100) NOT NULL',
		    'title' => Schema::TYPE_STRING . '(100) NOT NULL',
		    'keywords' => Schema::TYPE_STRING . '(128) NOT NULL',
		    'description' => Schema::TYPE_STRING . '(250) NOT NULL',
		    'text' => Schema::TYPE_TEXT . ' NOT NULL',
		    'status' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0',//по умолчанию черновик
		    'picture' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0',//по умолчанию картинки нет
		    'pic_in_post' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 1',//по умолчанию картинка показывается внутри поста
		    'site_id' => Schema::TYPE_SMALLINT . ' NOT NULL',
		    'publish_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->createIndex('post_tbl_publish_at_idx', 'post', 'publish_at');
    }

    public function down()
    {
        echo "m150118_153807_create_post_table cannot be reverted.\n";

        return false;
    }
}
