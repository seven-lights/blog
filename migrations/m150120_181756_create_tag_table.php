<?php

use yii\db\Schema;
use yii\db\Migration;

class m150120_181756_create_tag_table extends Migration
{
    public function up()
    {
	    $this->createTable('tag', [
		    'id' => Schema::TYPE_PK,
		    'name' => Schema::TYPE_STRING . '(50) NOT NULL',
		    'frequency' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1',
		    'site_id' => Schema::TYPE_SMALLINT . ' NOT NULL',
	    ]);
	    $this->createIndex('tag_tbl_frequency_idx', 'tag', 'frequency');
    }

    public function down()
    {
        echo "m150120_181756_create_tag_table cannot be reverted.\n";

        return false;
    }
}
