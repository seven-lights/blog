<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "vk_site".
 *
 * @property string $vk_id
 * @property integer $site_id
 *
 * @property Site $site
 */
class VkSite extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vk_site';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vk_id', 'site_id'], 'required'],
            [['vk_id', 'site_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vk_id' => 'Vk ID',
            'site_id' => 'Site ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }
}
