<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "site".
 *
 * @property integer $id
 * @property string $name
 * @property string $domain
 * @property integer $owner_id
 * @property string $mainpage
 *
 * @property Post[] $posts
 * @property Redactor[] $redactors
 * @property User[] $users
 * @property User $owner
 * @property Tag[] $tags
 * @property VkSite[] $vkSites
 */
class Site extends ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'site';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'domain'], 'required'],
            ['owner_id', 'integer'],
            [['name', 'domain'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'domain' => 'Домен (ник)',
            'owner_id' => 'ID владельца',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts() {
        return $this->hasMany(Post::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRedactors() {
        return $this->hasMany(Redactor::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('redactor', ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner() {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags() {
        return $this->hasMany(Tag::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVkSites() {
        return $this->hasMany(VkSite::className(), ['site_id' => 'id']);
    }
}
