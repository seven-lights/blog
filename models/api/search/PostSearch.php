<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\models\api\search;


use app\models\Post;
use general\controllers\api\Controller;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

class PostSearch extends Post {
	public function rules() {
		// only fields in rules() are searchable
		return [
			[['id', 'status'], 'integer'],
		];
	}

	public function search($params, $site_id, $page = 0) {
		$query = Post::find()
			->select(['id', 'title', 'publish_at', 'status'])
			->where(['site_id' => $site_id]);

		// load the search form data and validate
		if ($this->load($params) && $this->validate()) {
			$query->andFilterWhere(['id' => $this->id]);
			if($this->status == Post::STATUS_WAITING) {
				$query->andFilterWhere(['>', 'publish_at', time()])
					->andFilterWhere(['status' => Post::STATUS_WAITING]);
			} else {
				$query->andFilterWhere(['status' => $this->status]);
			}
		}

		$count_query = clone $query;
		$total_count = $count_query->limit(-1)->offset(-1)->orderBy([])->count();
		if($total_count == 0) {
			return Controller::ERROR_NO_POST;
		}
		$pages = new Pagination(['totalCount' => $total_count]);
		$pages->setPage($page - 1);
		$pages->setPageSize(30);

		$query->offset($pages->offset)
			->limit($pages->limit);

		if ($sort = \Yii::$app->request->get('sort')) {
			if(strncmp($sort, '-', 1) === 0) {
				$query->orderBy([substr($sort, 1) => SORT_DESC]);
			} else {
				$query->orderBy([$sort => SORT_ASC]);
			}
		}

		return [
			'posts' => $query->asArray()->all(),
			'totalCount' => $total_count,
			'page' => $pages->getPage()
		];
	}
}