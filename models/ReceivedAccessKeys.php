<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Полученные ключи
 *
 * @property string $access_key
 * @property string $service
 * @property integer $expires_in
 */
class ReceivedAccessKeys extends ActiveRecord {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['access_key', 'service', 'expires_in'], 'required'],
			[['expires_in'], 'integer'],
			[['access_key'], 'string', 'max' => 64],
			[['service'], 'string', 'max' => 20]
		];
	}
}
